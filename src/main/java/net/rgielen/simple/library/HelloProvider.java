package net.rgielen.simple.library;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public class HelloProvider {

    // sinnlos
    private String wirdNichtGenutzt;

    public String sayHelloTo(String name) {
        return "Hello " + name;
    }

    public void tuNix(String irgendwas) {
        System.out.println(irgendwas.length());
    }
}
